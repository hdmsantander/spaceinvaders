package main.java.com.uam.spaceinvaders;

import main.java.com.uam.spaceinvaders.presentation.GameFrame;

public class App {
	public static void main(String[] args) {
		new GameFrame();
	}
}
