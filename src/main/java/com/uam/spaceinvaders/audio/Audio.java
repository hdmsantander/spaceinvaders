package main.java.com.uam.spaceinvaders.audio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Audio {
	
	private Clip clip;
	private AudioInputStream audioInputStream = null;
	private String musicPath;
	private int playMode;

	public Audio(String musicPath, int playMode) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		this.musicPath = musicPath;
		this.playMode = playMode;
		initializeTrack();
	}

	public void initializeTrack()
			throws UnsupportedAudioFileException, IOException, LineUnavailableException {

		if (audioInputStream != null) {
			audioInputStream.close();
			clip.stop();
			clip.close();
		}

		this.clip = AudioSystem.getClip();

		InputStream is = Audio.class.getClassLoader().getResourceAsStream(musicPath);

		this.audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(is));

		this.clip.open(audioInputStream);
		
		this.clip.loop(playMode);
	}

	public void start() {
		clip.setFramePosition(0);
		clip.start();
	}
	
	public void stop() {
		clip.stop();
	}

}
