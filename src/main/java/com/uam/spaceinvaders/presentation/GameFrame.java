package main.java.com.uam.spaceinvaders.presentation;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class GameFrame extends JFrame {

	public static final int WIDTH = 1024;
	public static final int HEIGHT = 768;

	private Dimension dimensions;

	private static final long serialVersionUID = -3307506519359464480L;
	
	private GamePanel gamePanel = null;

	public GameFrame() {
		super();
		this.dimensions = new Dimension(WIDTH, HEIGHT);
		setMinimumSize(dimensions);
		setMaximumSize(dimensions);
		setPreferredSize(dimensions);
		setResizable(false);
		setTitle("~ SPACE INVADERS ~");
		setLocationRelativeTo(null);
		setBackground(Color.BLACK);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.gamePanel = new GamePanel(this);
		add(gamePanel);
		pack();
		setVisible(false);
		startGame();
	}
	
	public void startGame() {
		setVisible(true);
	}
	
	public int getMaximumWidth() {
		return WIDTH;
	}
	
	public int getMaximumHeight() {
		return HEIGHT - 30;
	}
	
}
