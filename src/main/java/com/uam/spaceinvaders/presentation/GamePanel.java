package main.java.com.uam.spaceinvaders.presentation;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import main.java.com.uam.spaceinvaders.entities.Spaceship;
import main.java.com.uam.spaceinvaders.levels.Level;
import main.java.com.uam.spaceinvaders.levels.LevelOne;

public class GamePanel extends JPanel implements ActionListener, KeyListener {

	private static final long serialVersionUID = 3119476954051942138L;

	private GameFrame gameFrame;
	
	private Level level;

	private Timer timer;
	
	private Spaceship spaceship;

	public GamePanel(GameFrame gameFrame) {
		this.gameFrame = gameFrame;
		this.spaceship = new Spaceship(this);
		this.timer = new Timer(1, this);
		this.addKeyListener(this);
		this.setFocusable(true);
		this.level = new LevelOne(this, "background.png", "levelone", 1024, 768, spaceship);
		
		this.startGame();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		level.paint(g);
		spaceship.paint(g);
	}

	public void startGame() {
		try {
			timer.start();
			level.start();
			spaceship.start();
			setVisible(true);
		} catch (IllegalThreadStateException e) {
			System.out.println("Failed starting game");
			e.printStackTrace();
		}
	}
	
	public void lostGame() {
		stopGame();
	}

	public void stopGame() {
		timer.stop();
	}

	private void update() {
		if (this.isDisplayable() && this.isValid()) {
			repaint();
		}
	}

	public Level getLevel() {
		return level;
	}

	public int getWidth() {
		return gameFrame.getMaximumWidth();
	}

	public int getHeight() {
		return gameFrame.getMaximumHeight();
	}

	public void keyPressed(KeyEvent event) {
		spaceship.keyPressed(event.getKeyCode());
	}

	public void keyReleased(KeyEvent event) {
		spaceship.keyReleased(event.getKeyCode());
	}

	public void keyTyped(KeyEvent arg0) {

	}

	public void actionPerformed(ActionEvent arg0) {
		update();
	}
	
	public void waitSomeTime(long milis, int nanos) {
		try {
			Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
