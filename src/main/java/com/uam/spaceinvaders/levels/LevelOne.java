package main.java.com.uam.spaceinvaders.levels;

import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import main.java.com.uam.spaceinvaders.audio.Audio;
import main.java.com.uam.spaceinvaders.entities.Sandbag;
import main.java.com.uam.spaceinvaders.entities.Spaceship;
import main.java.com.uam.spaceinvaders.entities.aliens.Alien;
import main.java.com.uam.spaceinvaders.entities.aliens.WhiteAlien;
import main.java.com.uam.spaceinvaders.presentation.GamePanel;

public class LevelOne extends Level {
	
	public LevelOne(GamePanel gamePanel, String levelBackground, String levelName, int backgroundWidth, int backgroundHeight, Spaceship spaceship) {
		
		super(gamePanel, levelBackground, levelName, backgroundWidth, backgroundHeight, spaceship);
		
		initializeSandbags();
		initializeAliens();
		
		try {
			setLevelAudio(new Audio("durante.wav",Clip.LOOP_CONTINUOUSLY));
		}catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			System.out.println("Can't play music");
		}
		
		playLevelAudio();
		
		
	}

	@Override
	public void moveAliens() {
				
		for (Alien a : aliens) {
			if (a.isAtLeftEdge()) {
				movingAliensLeft = false;
				break;
			}
		}
		
		for (Alien a: aliens) {
			if (a.isAtRightEdge()) {
				movingAliensLeft = true;
				break;
			}
		}
		
		if (movingAliensLeft) {
			moveAlienLeft();
		} else {
			moveAlienRight();
		}
		
	}
	
	

	@Override
	public void initializeSandbags() {
		
		int bagRows = 4;
		int bagColumns = 6;
				
		for (int i = 0 ; i < 4 ; i++) {
			
			int bagOffset = 100;
			int yOffset = gamePanel.getHeight()/5* 4-30;
			
			for (int j = 0 ; j < bagColumns ; j ++) {
				for (int k = 0 ; k < bagRows ; k ++) {
					sandbags.add(new Sandbag(bagOffset + (i*(bagOffset-25)*3) + j*(Sandbag.SANDBAG_WIDTH + 2), yOffset + k*(Sandbag.SANDBAG_HEIGHT + 2)));
				}
			}
		}
	}

	@Override
	public void initializeAliens() {
		
		int alienXOffset = 300;
		int alienYOffset = 50;
		
		for (int i = 0 ; i < 12; i++) {
			aliens.add(new WhiteAlien(alienXOffset + (Alien.ALIEN_WIDTH * i*2) ,alienYOffset, this));
		}
		
		
		
	}
	
	

}
