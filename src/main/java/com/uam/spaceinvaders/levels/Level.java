package main.java.com.uam.spaceinvaders.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import main.java.com.uam.spaceinvaders.audio.Audio;
import main.java.com.uam.spaceinvaders.entities.Sandbag;
import main.java.com.uam.spaceinvaders.entities.Spaceship;
import main.java.com.uam.spaceinvaders.entities.aliens.Alien;
import main.java.com.uam.spaceinvaders.entities.projectiles.Projectile;
import main.java.com.uam.spaceinvaders.presentation.GamePanel;

public abstract class Level extends Thread {
	
	private static final int ALIEN_MOVEMENT = 20;

	private BufferedImage gameImage;
	
	private boolean isActive;
	
	public GamePanel gamePanel;
	
	public String levelBackground;
	
	public String levelName;
	
	public List<Alien> aliens;
	
	public boolean movingAliensLeft;
	
	public List<Projectile> alienProjectiles;
	
	public List<Sandbag> sandbags;
	
	public Spaceship spaceship;
	
	public Audio levelAudio;
	
	public int backgroundWidth;
	
	public int backgroundHeight;
	
	public int horizontalMovements;
	
	public int horizontalTimer;
	
	public int verticalMovements;
	
	public int verticalTimer;
	
	public Level(GamePanel gamePanel, String levelBackground, String levelName, int backgroundWidth, int backgroundHeight, Spaceship spaceship) {
		this.isActive = true;
		this.gamePanel = gamePanel;
		this.spaceship = spaceship;
		this.movingAliensLeft = true;
		this.levelBackground = levelBackground;
		this.gameImage = initializeBackgroundImage();
		this.backgroundWidth = backgroundWidth;
		this.sandbags = new ArrayList<>();
		this.aliens = new ArrayList<>();
		this.gameImage = initializeBackgroundImage();
		this.backgroundHeight = backgroundHeight;
		this.levelName = levelName;
	}
	
	public abstract void moveAliens();
	
	public abstract void initializeSandbags();
	
	public abstract void initializeAliens(); 
	
	public boolean hitsSandbag(Projectile p) {
		
		for (Iterator<Sandbag> it = sandbags.iterator() ; it.hasNext() ;) {
			Sandbag s = it.next();
			if (p.intersects(s.getBounds())) {
				it.remove();
				return true;
			}
		}
		
		return false;
	}
	
	public void paint(Graphics g) {
		
		g.drawImage(gameImage, 0, 0, gamePanel);
		
		for(Iterator<Sandbag> it = sandbags.iterator(); it.hasNext() ; ) {
			Sandbag s = it.next();
			s.paint(g);
		}
		
		for(Iterator<Alien> it = aliens.iterator(); it.hasNext() ; ) {
			Alien a = it.next();
			a.paint(g);
			if (wasHitByBullet(a)) {
				it.remove();
			}
		}
	}
	
	private boolean wasHitByBullet(Alien a) {
		
		for (Iterator<Projectile> it = spaceship.getProjectiles().iterator(); it.hasNext();) {
			
			Projectile p = it.next();
			
			if (p.intersects(a.getBounds())) {
				it.remove();
				return true;
			}
		
		}
		
		return false;
		
	}
	
	@Override
	public void run() {
		spaceship.setPosition(gamePanel.getWidth()/2 - Spaceship.SHIP_WIDTH, gamePanel.getHeight()-Spaceship.SHIP_HEIGHT-10);
		update();
	}
	
	private void update() {
		while(isActive) {
			waitSomeTime(200, 0);
			moveAliens();
		}
	}
		
	public GamePanel getGamePanel() {
		return this.gamePanel;
	}
	
	public void setLevelAudio(Audio l) {
		levelAudio = l;
	}
	
	public void playLevelAudio() {
		levelAudio.start();
	}
	
	public void moveAlienLeft() {
		for(Alien a : aliens) {
			a.setPosition(a.x - ALIEN_MOVEMENT, a.y);
		}
	}
	
	public void moveAlienRight() {
		for(Alien a : aliens) {
			a.setPosition(a.x + ALIEN_MOVEMENT, a.y);
		}
	}
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(levelBackground));
		} catch (IOException e) {
			return null;
		}
	}
	
	public void waitSomeTime(long milis, int nanos) {
		try {
			Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
