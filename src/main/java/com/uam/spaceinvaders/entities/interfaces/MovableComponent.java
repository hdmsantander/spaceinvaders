package main.java.com.uam.spaceinvaders.entities.interfaces;

public interface MovableComponent {
	
	boolean canMoveLeft();
	
	void moveLeft();

	boolean canMoveRight();
	
	void moveRight();
	
	void move();

}
