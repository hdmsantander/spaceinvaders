package main.java.com.uam.spaceinvaders.entities.aliens;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;
import java.util.Random;

import javax.imageio.ImageIO;
import main.java.com.uam.spaceinvaders.levels.Level;
import main.java.com.uam.spaceinvaders.presentation.GamePanel;

public abstract class Alien {
	
	public static final int ALIEN_WIDTH = 20;
	
	public static final int ALIEN_HEIGHT = 20;
	
	public static final int ALIEN_EDGES = 40;
	
	public int x;
	
	public int y;
	
	private Level level;
	
	private BufferedImage gameImage;
	
	private Optional<BufferedImage> alternateGameImage;
	
	private String spritePath;
	
	private Optional<String> alternateSpritePath;
	
	private boolean firstSprite;
	
	private int spriteTimer;
	
	private Random random;
			
	public Alien(int x, int y, String spritePath, Optional<String> alternateSpritePath, Level level) {
		this.x = x;
		this.y = y;
		this.spritePath = spritePath;
		this.alternateSpritePath = alternateSpritePath;
		this.level = level;
		this.random = new Random();
		this.firstSprite = true;
		
		this.gameImage = initializeBackgroundImage();
		
		if (alternateSpritePath.isPresent()) {
			this.alternateGameImage = initializeAlternateBackgroundImage();
		}
	}
	
	public Random getRandom() {
		return random;
	}
	
	public Level getLevel() {
		return level;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isAtLeftEdge() {
		return x <= ALIEN_EDGES;
	}
	
	public boolean isAtRightEdge() {
		return x + ALIEN_EDGES *2>= level.getGamePanel().getWidth() ;
	}
	
	public boolean intersects(Rectangle r) {
		return getBounds().intersects(r);
	}
	
	public void paint(Graphics g) {
		
		if (spriteTimer == 0) {
			firstSprite = !firstSprite;
			spriteTimer = 0;
		}
		else {
			spriteTimer = (spriteTimer+1)%150;
		}
			
		
		if (alternateGameImage.isEmpty())
		{
			g.drawImage(gameImage, x, y, ALIEN_WIDTH, ALIEN_HEIGHT, level.getGamePanel());
			
		} else {
			if (firstSprite) {
				g.drawImage(gameImage, x, y, ALIEN_WIDTH, ALIEN_HEIGHT, level.getGamePanel());

			} else {

				g.drawImage(alternateGameImage.get(), x, y, ALIEN_WIDTH, ALIEN_HEIGHT, level.getGamePanel());
			}
		}
		
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x,y,ALIEN_WIDTH,ALIEN_HEIGHT);
	}
	
	public abstract boolean willTheAlienShoot();
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(spritePath));
		} catch (IOException e) {
			return null;
		}
	}
	
	private Optional<BufferedImage> initializeAlternateBackgroundImage() {
		try {
			return Optional.of(ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(spritePath)));
		} catch (IOException e) {
			return Optional.empty();
		}
	}

}
