package main.java.com.uam.spaceinvaders.entities.aliens;

import java.util.Optional;

import main.java.com.uam.spaceinvaders.levels.Level;

public class GreenAlien extends Alien {
	
	public GreenAlien(int x, int y,Level level) {
		super(x,y,"marciano1.png",Optional.empty(),level);
	}

	@Override
	public boolean willTheAlienShoot()  {
		return getRandom().nextInt(20) == 0;
	}

}
