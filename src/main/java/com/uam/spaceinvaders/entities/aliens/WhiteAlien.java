package main.java.com.uam.spaceinvaders.entities.aliens;

import java.util.Optional;

import main.java.com.uam.spaceinvaders.levels.Level;

public class WhiteAlien extends Alien {
	
	public WhiteAlien(int x, int y,Level level) {
		super(x,y,"alien3_0.png",Optional.of("alien3_1.png"),level);
	}

	@Override
	public boolean willTheAlienShoot()  {
		return getRandom().nextInt(20) == 0;
	}
}
