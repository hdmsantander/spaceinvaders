package main.java.com.uam.spaceinvaders.entities.projectiles;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.java.com.uam.spaceinvaders.presentation.GamePanel;

public abstract class Projectile {
	
	public final static int PROJECTILE_WITDH = 5;
	
	public final static int PROJECTILE_HEIGHT = 5;
	
	private int x;
	
	private int y;
	
	private int xIncrement;
	
	private int yIncrement;
	
	private GamePanel gamePanel;
	
	private BufferedImage gameImage;
	
	private String spritePath;
		
	public Projectile(int x, int y, int xIncrement, int yIncrement, String spritePath, GamePanel gamePanel) {
		this.x = x;
		this.y = y;
		this.xIncrement = xIncrement;
		this.yIncrement = yIncrement;
		this.spritePath = spritePath;
		this.gamePanel = gamePanel;
		this.gameImage = initializeBackgroundImage();
	}
	
	public void paint(Graphics g) {
		g.drawImage(gameImage, x, y, PROJECTILE_WITDH, PROJECTILE_HEIGHT, gamePanel);
	}
	
	public void updatePosition() {
		x += xIncrement;
		y += yIncrement;
	}
	
	public abstract boolean isOutOfBounds();
	
	public boolean intersects(Rectangle r) {
		return getBounds().intersects(r);
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public GamePanel getGamePanel() {
		return gamePanel;
	}
	
	private Rectangle getBounds() {
		return new Rectangle(x,y,PROJECTILE_WITDH,PROJECTILE_HEIGHT);
	}
	
	private BufferedImage initializeBackgroundImage() {
		
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(spritePath));
		} catch (IOException e) {
			return null;
		}
	}

}
