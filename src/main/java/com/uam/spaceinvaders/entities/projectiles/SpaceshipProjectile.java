package main.java.com.uam.spaceinvaders.entities.projectiles;

import main.java.com.uam.spaceinvaders.entities.Spaceship;

public class SpaceshipProjectile extends Projectile {
	
	private Spaceship spaceship;
	
	public SpaceshipProjectile(int x, int y, int xIncrement, int yIncrement, Spaceship spaceship) {
		super(x, y, xIncrement, yIncrement, "bala.png", spaceship.getGamePanel());
	}

	@Override
	public boolean isOutOfBounds() {
		return getY() < 0;
	}

}
