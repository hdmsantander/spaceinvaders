package main.java.com.uam.spaceinvaders.entities.projectiles;

import main.java.com.uam.spaceinvaders.entities.aliens.Alien;

public class AlienProjectile extends Projectile {
	
	public AlienProjectile(int x, int y, int xIncrement, int yIncrement, Alien alien) {
		super(x, y, xIncrement, yIncrement, "bala-enemigo", alien.getLevel().getGamePanel());
	}

	@Override
	public boolean isOutOfBounds() {
		return getY() > getGamePanel().getHeight();
	}

}
