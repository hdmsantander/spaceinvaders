package main.java.com.uam.spaceinvaders.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import main.java.com.uam.spaceinvaders.audio.Audio;
import main.java.com.uam.spaceinvaders.entities.interfaces.MovableComponent;
import main.java.com.uam.spaceinvaders.entities.projectiles.Projectile;
import main.java.com.uam.spaceinvaders.entities.projectiles.SpaceshipProjectile;
import main.java.com.uam.spaceinvaders.presentation.GamePanel;

public class Spaceship extends Thread implements MovableComponent {
	
	public static final int SHIP_WIDTH = 40;
	
	public static final int SHIP_HEIGHT = 40;
	
	public static final int MOVING_SPEED = 5;
	
	private static final int MOVEMENT_DELAY = 4;
	
	private static final int SHOOTING_DELAY = 130;
	
	private boolean isActive;
	
	private BufferedImage gameImage;
	
	private boolean canShoot;
	
	private int shootingTimer;
	
	private String spritePath;
	
	private int damageLevel;
	
	private GamePanel gamePanel;
	
	private List<Projectile> projectiles;
	
	private Audio projectileAudio;
	
	private Audio damageAudio;
	
	private int x;
	
	private int y;
	
	private boolean movingLeft;
	
	private int horizontalTimer;
	
	private boolean movingRight;
		
	public Spaceship(GamePanel gamePanel) {
		try {
			this.projectileAudio = new Audio("projectile.wav",0);
			this.damageAudio = new Audio("impact.wav",0);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			System.out.println("Couldn't load sounds");
		}
		this.gamePanel = gamePanel;
		this.damageLevel = 0;
		this.horizontalTimer = 0;
		this.shootingTimer = 0;
		this.canShoot = true;
		this.spritePath = "nave1.png";
		this.gameImage = initializeBackgroundImage();
		this.projectiles = new ArrayList<>();
		this.isActive = true;				
	}
	
	@Override
	public void run() {
		update();
	}
	
	public void wasHit() {
		damageAudio.start();
		damageLevel++;
		if(damageLevel == 3) {
			gamePanel.lostGame();
		}
	}
	
	private void update() {
		while(isActive) {
			
			waitSomeTime(9, 0);
			
			shootingTimer = (shootingTimer + 1) % SHOOTING_DELAY;
			
			if(shootingTimer == SHOOTING_DELAY -1) {
				canShoot = true;
				shootingTimer = 0;
			}
			
			move();
		}
	}
	
	public void paint(Graphics g) {
		
		g.drawImage(gameImage, x, y, SHIP_WIDTH, SHIP_HEIGHT, gamePanel);
		
		switch(damageLevel) {
		
		case 1:
			g.setColor(Color.RED);
			g.fillRect(x + SHIP_WIDTH/2 - 10, y + SHIP_HEIGHT/2 - 4, 3, 3);
			g.fillRect(x + SHIP_WIDTH/2 + 13, y + SHIP_HEIGHT/2 + 6, 3, 3);
			break;
		case 2:
			g.setColor(Color.RED);
			g.fillRect(x + SHIP_WIDTH/2 - 10, y + SHIP_HEIGHT/2 - 4, 3, 3);
			g.fillRect(x + SHIP_WIDTH/2 + 13, y + SHIP_HEIGHT/2 + 6, 3, 3);
			g.fillRect(x + SHIP_WIDTH/2, y + SHIP_HEIGHT/2 -12, 4, 4);
			g.fillRect(x + SHIP_WIDTH/2, y + SHIP_HEIGHT/2 +12, 4, 4);
			g.fillRect(x + SHIP_WIDTH/2-3, y + SHIP_HEIGHT/2 + 1, 3, 3);
			g.fillRect(x + SHIP_WIDTH/2 +5, y + SHIP_HEIGHT/2 - 4, 3, 3);
			break;
		}
		
		
		for (Iterator<Projectile> it =projectiles.iterator(); it.hasNext() ;) {
			Projectile p = it.next();
			p.paint(g);
			if (p.isOutOfBounds() || gamePanel.getLevel().hitsSandbag(p)) {
				it.remove();
			}
		}
		
		for (Projectile p : projectiles) {
			p.paint(g);
		}
	}
	
	public void keyPressed(int keyCode) {
		
		switch(keyCode) {
		
		case KeyEvent.VK_LEFT:
			movingRight = false;
			movingLeft = true;
			break;

		case KeyEvent.VK_RIGHT:
			movingLeft = false;
			movingRight = true;
			break;
			
		case KeyEvent.VK_UP:
			if(canShoot) {
				shoot();
				canShoot = false;
			}
				
			break;
					
		}
		
	}
	
	public GamePanel getGamePanel() {
		return gamePanel;
	}
	
	public void removeProjectile(Projectile p) {
		projectiles.remove(p);
	}
	
	public void keyReleased(int keyCode) {
		
		switch(keyCode) {
		
		case KeyEvent.VK_RIGHT:
			movingRight = false;
			break;
			
		case KeyEvent.VK_LEFT:
			movingLeft = false;
			break;
		}
		
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
		
	@Override
	public boolean canMoveLeft() {
		return x - MOVING_SPEED >= 0;
	}

	@Override
	public void moveLeft() {
		x-=MOVING_SPEED;		
	}

	@Override
	public boolean canMoveRight() {
		return x+MOVING_SPEED + SHIP_WIDTH <= gamePanel.getWidth(); 
	}

	@Override
	public void moveRight() {
		x+=MOVING_SPEED;		
	}
	
	public List<Projectile> getProjectiles(){
		return projectiles;
	}

	@Override
	public void move() {
		

		for (Projectile p : projectiles) {
			p.updatePosition();
		}
		
		if (horizontalTimer != MOVEMENT_DELAY -1) {
			horizontalTimer = (horizontalTimer +1) % MOVEMENT_DELAY;
			return;
		} else {
			horizontalTimer = 0;
		}
		
		if (movingRight && canMoveRight()) {
			moveRight();
		}
		
		if (movingLeft && canMoveLeft()) {
			moveLeft();
		}
		
		
	}
	
	private void shoot() {
		projectiles.add(new SpaceshipProjectile(x+SHIP_WIDTH/2-3,y-Projectile.PROJECTILE_HEIGHT-4,0,-2,this));
		projectileAudio.start();
	}
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(spritePath));
		} catch (IOException e) {
			return null;
		}
	}
	
	private void waitSomeTime(long milis, int nanos) {
		try {
			Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
