package main.java.com.uam.spaceinvaders.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Sandbag {
	
	public static final int SANDBAG_WIDTH = 20;
	
	public static final int SANDBAG_HEIGHT = 20;
	
	private static final Color SANDBAG_COLOR = Color.PINK;
	
	private int x;
	
	private int y;
	
	public Sandbag(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean intersects(Rectangle r) {
		return getBounds().intersects(r);
	}
	
	public void paint(Graphics g) {
		g.setColor(SANDBAG_COLOR);
		g.fillRoundRect(x, y, SANDBAG_WIDTH, SANDBAG_HEIGHT, 10, 10);
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x,y,SANDBAG_WIDTH,SANDBAG_HEIGHT);
	}
	
}
